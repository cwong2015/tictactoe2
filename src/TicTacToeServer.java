import java.net.ServerSocket;
import java.io.IOException;
import java.net.InetAddress;

public class TicTacToeServer {
    public static TicTacToeGame game;

    public TicTacToeServer() {

    }

    private static void initGame(ServerSocket serverSocket) {
        try {
            while (true) {
                game = new TicTacToeGame();
                Player player1 = null;
                try {
                    player1 = new Player(serverSocket.accept(), 'X');
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Player player2 = null;
                try {
                    player2 = new Player(serverSocket.accept(), 'O');
                } catch (IOException e) {
                    e.printStackTrace();
                }
                player1.setOpponent(player2);
                player2.setOpponent(player1);
                player2.setGame(game);
                player1.setGame(game);
                game.setCurrentPlayer(player2);

                player2.start();
                player1.start();
            }
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void connectServer(int port, String ip) {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port, 8, InetAddress.getByName(ip));
            System.out.println("STATUS 200, CONNECTION SUCCESS");
            initGame(serverSocket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        TicTacToeServer server = new TicTacToeServer();
        server.connectServer(12349, "localhost");
    }
}